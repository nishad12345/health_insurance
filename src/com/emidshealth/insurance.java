package com.emidshealth;

import java.io.BufferedReader;
import java.io.InputStreamReader;


public class insurance {

	public static final int BASE_PREMIUM = 5000;

	public static double calculateInsurance(Person p) {

		PersonTestService person = new PersonTestService();

		return BASE_PREMIUM + person.getPersonAgePremiumAmount(p, BASE_PREMIUM)
				+ person.getPersonGenderPremiumAmount(p, BASE_PREMIUM)
				+ person.getPersonHabitsPremiumAmount(p, BASE_PREMIUM)
				+ person.getPersonHealthPremiumAmount(p, BASE_PREMIUM);

	}

	public static void main(String[] args) {

		Person person = new Person();
		String gender = "";
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		try {
			System.out.println("Enter Customer Name :");
			person.setName(br.readLine());
			System.out.println("Enter Gender (Male,Female,Other):");
			gender = br.readLine();
			System.out.println("Enter Age:");
			person.setAge(Integer.parseInt(br.readLine()));
			System.out.println("**Your Current Health**");
			System.out.println("HyperTension(Yes/No):");
			person.getCurrenthealth().setHypertension(
					"Yes".equalsIgnoreCase(br.readLine()) ? true : false);
			System.out.println("BloodPressure(Yes/No):");
			person.getCurrenthealth().setBloodpressure(
					"Yes".equalsIgnoreCase(br.readLine()) ? true : false);
			System.out.println("BloodSugar(Yes/No):");
			person.getCurrenthealth().setBloodsugar(
					"Yes".equalsIgnoreCase(br.readLine()) ? true : false);
			System.out.println("OverWeight(Yes/No):");
			person.getCurrenthealth().setOverweight(
					"Yes".equalsIgnoreCase(br.readLine()) ? true : false);

			System.out.println("**Your Habits**");
			System.out.println("Smoke(Yes/No):");
			person.getHabit().setSmoking(
					"Yes".equalsIgnoreCase(br.readLine()) ? true : false);
			System.out.println("Alcohol(Yes/No):");
			person.getHabit().setAlcohol(
					"Yes".equalsIgnoreCase(br.readLine()) ? true : false);
			System.out.println("Daily Exercise(Yes/No):");
			person.getHabit().setExercise(
					"Yes".equalsIgnoreCase(br.readLine()) ? true : false);
			System.out.println("Drugs(Yes/No):");
			person.getHabit().setDrugs(
					"Yes".equalsIgnoreCase(br.readLine()) ? true : false);

			if (gender.equalsIgnoreCase("Male")) {
				person.setGender(Gender.Male);
			} else if (gender.equalsIgnoreCase("Female")) {
				person.setGender(Gender.Female);
			} else {
				person.setGender(Gender.other);
			}

			System.out.println("Health Insurance Premium for Mr."
					+ person.getName() + ": Rs." + calculateInsurance(person));

		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}

	}

}
