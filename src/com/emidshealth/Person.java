package com.emidshealth;


public class Person {

	private String name;
	private int age;
	private Health currenthealth = new Health();
	private Habits habit = new Habits();
	Gender gender;

	public double AgePremiumAmount;
	public double GenderPremiumAmount;
	public double healthPremiumAmount;
	public double habitsPremiumAmount;

	public double getAgePremiumAmount() {
		return AgePremiumAmount;
	}

	public void setAgePremiumAmount(double agePremiumAmount) {
		AgePremiumAmount = agePremiumAmount;
	}

	public double getGenderPremiumAmount() {
		return GenderPremiumAmount;
	}

	public void setGenderPremiumAmount(double genderPremiumAmount) {
		GenderPremiumAmount = genderPremiumAmount;
	}

	public double getHealthPremiumAmount() {
		return healthPremiumAmount;
	}

	public void setHealthPremiumAmount(double healthPremiumAmount) {
		this.healthPremiumAmount = healthPremiumAmount;
	}

	public double getHabitsPremiumAmount() {
		return habitsPremiumAmount;
	}

	public void setHabitsPremiumAmount(double habitsPremiumAmount) {
		this.habitsPremiumAmount = habitsPremiumAmount;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public Health getCurrenthealth() {
		return currenthealth;
	}

	public void setCurrenthealth(Health currenthealth) {
		this.currenthealth = currenthealth;
	}

	public Habits getHabit() {
		return habit;
	}

	public void setHabit(Habits habit) {
		this.habit = habit;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

}
