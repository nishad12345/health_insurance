package com.emidshealth;

public class Health {

	private boolean hypertension;
	private boolean bloodpressure;
	private boolean bloodsugar;
	private boolean overweight;
	
	
	public boolean isHypertension() {
		return hypertension;
	}
	public void setHypertension(boolean hypertension) {
		this.hypertension = hypertension;
	}
	public boolean isBloodpressure() {
		return bloodpressure;
	}
	public void setBloodpressure(boolean bloodpressure) {
		this.bloodpressure = bloodpressure;
	}
	public boolean isBloodsugar() {
		return bloodsugar;
	}
	public void setBloodsugar(boolean bloodsugar) {
		this.bloodsugar = bloodsugar;
	}
	public boolean isOverweight() {
		return overweight;
	}
	public void setOverweight(boolean overweight) {
		this.overweight = overweight;
	}
	
	

}
